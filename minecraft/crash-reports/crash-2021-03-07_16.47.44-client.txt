---- Minecraft Crash Report ----

WARNING: coremods are present:
  Regeneration (Regeneration-1.12.2-3.0.2.jar)
  Inventory Tweaks Coremod (InventoryTweaks-1.63.jar)
  MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.281.jar)
  LoadingPlugin (ResourceLoader-MC1.12.1-1.5.3.jar)
Contact their authors BEFORE contacting forge

// Don't be sad, have a hug! <3

Time: 3/7/21 4:47 PM
Description: Rendering Block Entity

java.lang.ClassCastException: com.mjr.extraplanets.tileEntities.blocks.TileEntityTier3LandingPadSingle cannot be cast to net.tardis.mod.common.tileentity.TileEntityTardis
	at net.tardis.mod.client.renderers.decorations.hellbent.RenderHellbentMonitor.render(RenderHellbentMonitor.java:43)
	at net.tardis.mod.client.renderers.decorations.hellbent.RenderHellbentMonitor.func_192841_a(RenderHellbentMonitor.java:20)
	at net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher.func_192854_a(TileEntityRendererDispatcher.java:156)
	at net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher.func_180546_a(TileEntityRendererDispatcher.java:131)
	at net.minecraft.client.renderer.RenderGlobal.func_180446_a(RenderGlobal.java:705)
	at net.minecraft.client.renderer.EntityRenderer.func_175068_a(EntityRenderer.java:1347)
	at net.minecraft.client.renderer.EntityRenderer.func_78471_a(EntityRenderer.java:1259)
	at net.minecraft.client.renderer.EntityRenderer.func_181560_a(EntityRenderer.java:1062)
	at net.minecraft.client.Minecraft.func_71411_J(Minecraft.java:1119)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:398)
	at net.minecraft.client.main.Main.main(SourceFile:123)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:196)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:231)
	at org.multimc.EntryPoint.listen(EntryPoint.java:143)
	at org.multimc.EntryPoint.main(EntryPoint.java:34)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- Head --
Thread: Client thread
Stacktrace:
	at net.tardis.mod.client.renderers.decorations.hellbent.RenderHellbentMonitor.render(RenderHellbentMonitor.java:43)
	at net.tardis.mod.client.renderers.decorations.hellbent.RenderHellbentMonitor.func_192841_a(RenderHellbentMonitor.java:20)

-- Block Entity Details --
Details:
	Name: tardis:tileentityhellbentmonitor // net.tardis.mod.common.tileentity.decoration.TileEntityHellbentMonitor
	Block type: ID #1066 (tile.tardis.hellbent_monitor // net.tardis.mod.common.blocks.BlockMonitor // tardis:hellbent_monitor)
	Block data value: 0 / 0x0 / 0b0000
	Block location: World: (264,129,272), Chunk: (at 8,8,0 in 16,17; contains blocks 256,0,272 to 271,255,287), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Actual block type: ID #1066 (tile.tardis.hellbent_monitor // net.tardis.mod.common.blocks.BlockMonitor // tardis:hellbent_monitor)
	Actual block data value: 0 / 0x0 / 0b0000
Stacktrace:
	at net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher.func_192854_a(TileEntityRendererDispatcher.java:156)
	at net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher.func_180546_a(TileEntityRendererDispatcher.java:131)
	at net.minecraft.client.renderer.RenderGlobal.func_180446_a(RenderGlobal.java:705)
	at net.minecraft.client.renderer.EntityRenderer.func_175068_a(EntityRenderer.java:1347)
	at net.minecraft.client.renderer.EntityRenderer.func_78471_a(EntityRenderer.java:1259)

-- Affected level --
Details:
	Level name: MpServer
	All players: 1 total; [GCEntityClientPlayerMP['LLAF_Teamkiwi'/115815, l='MpServer', x=262.39, y=130.30, z=263.16]]
	Chunk stats: MultiplayerChunkCache: 441, 441
	Level seed: 0
	Level generator: ID 00 - default, ver 1. Features enabled: false
	Level generator options: 
	Level spawn location: World: (0,65,0), Chunk: (at 0,4,0 in 0,0; contains blocks 0,0,0 to 15,255,15), Region: (0,0; contains chunks 0,0 to 31,31, blocks 0,0,0 to 511,255,511)
	Level time: 569149 game time, 582876 day time
	Level dimension: 0
	Level storage version: 0x00000 - Unknown?
	Level weather: Rain time: 0 (now: false), thunder time: 0 (now: false)
	Level game mode: Game mode: creative (ID 1). Hardcore: false. Cheats: false
	Forced entities: 2 total; [GCEntityClientPlayerMP['LLAF_Teamkiwi'/115815, l='MpServer', x=262.39, y=130.30, z=263.16], ControlDoor['Door'/115816, l='MpServer', x=257.50, y=128.00, z=249.50]]
	Retry entities: 0 total; []
	Server brand: Mohist
	Server type: Non-integrated multiplayer server
Stacktrace:
	at net.minecraft.client.multiplayer.WorldClient.func_72914_a(WorldClient.java:420)
	at net.minecraft.client.Minecraft.func_71396_d(Minecraft.java:2741)
	at net.minecraft.client.Minecraft.func_99999_d(Minecraft.java:419)
	at net.minecraft.client.main.Main.main(SourceFile:123)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
	at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
	at java.lang.reflect.Method.invoke(Unknown Source)
	at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:196)
	at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:231)
	at org.multimc.EntryPoint.listen(EntryPoint.java:143)
	at org.multimc.EntryPoint.main(EntryPoint.java:34)

-- System Details --
Details:
	Minecraft Version: 1.12.2
	Operating System: Windows 10 (amd64) version 10.0
	Java Version: 1.8.0_261, Oracle Corporation
	Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
	Memory: 1559775120 bytes (1487 MB) / 3798466560 bytes (3622 MB) up to 6681526272 bytes (6372 MB)
	JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms520m -Xmx7168m
	IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
	FML: MCP 9.42 Powered by Forge 14.23.5.2855 52 mods loaded, 52 mods active
	States: 'U' = Unloaded 'L' = Loaded 'C' = Constructed 'H' = Pre-initialized 'I' = Initialized 'J' = Post-initialized 'A' = Available 'D' = Disabled 'E' = Errored

	| State  | ID                  | Version                  | Source                                            | Signature                                |
	|:------ |:------------------- |:------------------------ |:------------------------------------------------- |:---------------------------------------- |
	| LCHIJA | minecraft           | 1.12.2                   | minecraft.jar                                     | None                                     |
	| LCHIJA | mcp                 | 9.42                     | minecraft.jar                                     | None                                     |
	| LCHIJA | FML                 | 8.0.99.99                | forge-1.12.2-14.23.5.2855-universal.jar           | e3c3d50c7c986df74c645c0ac54639741c90a557 |
	| LCHIJA | forge               | 14.23.5.2855             | forge-1.12.2-14.23.5.2855-universal.jar           | e3c3d50c7c986df74c645c0ac54639741c90a557 |
	| LCHIJA | micdoodlecore       |                          | minecraft.jar                                     | None                                     |
	| LCHIJA | aether_legacy       | 1.4.4                    | aether_legacy-1.12.2-v1.4.4.jar                   | None                                     |
	| LCHIJA | jei                 | 4.16.1.301               | jei_1.12.2-4.16.1.301.jar                         | None                                     |
	| LCHIJA | appleskin           | 1.0.14                   | AppleSkin-mc1.12-1.0.14.jar                       | None                                     |
	| LCHIJA | betterbuilderswands | 0.13.2                   | BetterBuildersWands-1.12.2-0.13.2.271+5997513.jar | None                                     |
	| LCHIJA | codechickenlib      | 3.2.3.358                | CodeChickenLib-1.12.2-3.2.3.358-universal.jar     | f1850c39b2516232a2108a7bd84d1cb5df93b261 |
	| LCHIJA | redstoneflux        | 2.1.1                    | RedstoneFlux-1.12-2.1.1.1-universal.jar           | None                                     |
	| LCHIJA | cofhcore            | 4.6.6                    | CoFHCore-1.12.2-4.6.6.1-universal.jar             | None                                     |
	| LCHIJA | cofhworld           | 1.4.0                    | CoFHWorld-1.12.2-1.4.0.1-universal.jar            | None                                     |
	| LCHIJA | mantle              | 1.12-1.3.3.55            | Mantle-1.12-1.3.3.55.jar                          | None                                     |
	| LCHIJA | twilightforest      | 3.11.1021                | twilightforest-1.12.2-3.11.1021-universal.jar     | None                                     |
	| LCHIJA | tconstruct          | 1.12.2-2.13.0.183        | TConstruct-1.12.2-2.13.0.183.jar                  | None                                     |
	| LCHIJA | conarm              | 1.2.5.10                 | conarm-1.12.2-1.2.5.10.jar                        | b33d2c8df492beff56d1bbbc92da49b8ab7345a1 |
	| LCHIJA | custommainmenu      | 2.0.9.1                  | CustomMainMenu-MC1.12.2-2.0.9.1.jar               | None                                     |
	| LCHIJA | enderstorage        | 2.4.6.137                | EnderStorage-1.12.2-2.4.6.137-universal.jar       | f1850c39b2516232a2108a7bd84d1cb5df93b261 |
	| LCHIJA | galacticraftcore    | 4.0.2.281                | GalacticraftCore-1.12.2-4.0.2.281.jar             | None                                     |
	| LCHIJA | galacticraftplanets | 4.0.2.281                | Galacticraft-Planets-1.12.2-4.0.2.281.jar         | None                                     |
	| LCHIJA | mjrlegendslib       | 1.12.2-1.2.0             | MJRLegendsLib-1.12.2-1.2.0.jar                    | b02331787272ec3515ebe63ecdeea0d746653468 |
	| LCHIJA | extraplanets        | 1.12.2-0.7.1             | ExtraPlanets-1.12.2-0.7.1.jar                     | b02331787272ec3515ebe63ecdeea0d746653468 |
	| LCHIJA | ftbbackups          | 1.1.0.1                  | FTBBackups-1.1.0.1.jar                            | None                                     |
	| LCHIJA | ftblib              | 5.4.7.2                  | FTBLib-5.4.7.2.jar                                | None                                     |
	| LCHIJA | ftbutilities        | 5.4.1.131                | FTBUtilities-5.4.1.131.jar                        | None                                     |
	| LCHIJA | waila               | 1.8.26                   | Hwyla-1.8.26-B41_1.12.2.jar                       | None                                     |
	| LCHIJA | lunatriuscore       | 1.2.0.42                 | LunatriusCore-1.12.2-1.2.0.42-universal.jar       | None                                     |
	| LCHIJA | ingameinfoxml       | 2.8.2.94                 | InGameInfoXML-1.12.2-2.8.2.94-universal.jar       | None                                     |
	| LCHIJA | inventorytweaks     | 1.63+release.109.220f184 | InventoryTweaks-1.63.jar                          | 55d2cd4f5f0961410bf7b91ef6c6bf00a766dcbe |
	| LCHIJA | journeymap          | 1.12.2-5.7.1             | journeymap-1.12.2-5.7.1.jar                       | None                                     |
	| LCHIJA | mob_grinding_utils  | 0.3.13                   | MobGrindingUtils-0.3.13.jar                       | None                                     |
	| LCHIJA | moreoverlays        | 1.15.1                   | moreoverlays-1.15.1-mc1.12.2.jar                  | None                                     |
	| LCHIJA | mousetweaks         | 2.10                     | MouseTweaks-2.10-mc1.12.2.jar                     | None                                     |
	| LCHIJA | natura              | 1.12.2-4.3.2.69          | natura-1.12.2-4.3.2.69.jar                        | None                                     |
	| LCHIJA | oreexcavation       | 1.4.150                  | OreExcavation-1.4.150.jar                         | None                                     |
	| LCHIJA | harvestcraft        | 1.12.2zb                 | Pam's HarvestCraft 1.12.2zg.jar                   | None                                     |
	| LCHIJA | quickleafdecay      | 1.2.4                    | QuickLeafDecay-MC1.12.1-1.2.4.jar                 | None                                     |
	| LCHIJA | refinedstorage      | 1.6.16                   | refinedstorage-1.6.16.jar                         | 57893d5b90a7336e8c63fe1c1e1ce472c3d59578 |
	| LCHIJA | tardis              | 0.1.4A                   | tardis-0.1.4A.jar                                 | None                                     |
	| LCHIJA | regeneration        | 3.0.2                    | Regeneration-1.12.2-3.0.2.jar                     | None                                     |
	| LCHIJA | resourceloader      | 1.5.3                    | ResourceLoader-MC1.12.1-1.5.3.jar                 | d72e0dd57935b3e9476212aea0c0df352dd76291 |
	| LCHIJA | thermalfoundation   | 2.6.7                    | ThermalFoundation-1.12.2-2.6.7.1-universal.jar    | None                                     |
	| LCHIJA | thermalexpansion    | 5.5.7                    | ThermalExpansion-1.12.2-5.5.7.1-universal.jar     | None                                     |
	| LCHIJA | thermaldynamics     | 2.5.6                    | ThermalDynamics-1.12.2-2.5.6.1-universal.jar      | None                                     |
	| LCHIJA | simplyjetpacks      | 1.12.2-2.2.18.1          | SimplyJetpacks2-1.12.2-2.2.18.1.jar               | None                                     |
	| LCHIJA | solarflux           | 12.4.11                  | SolarFluxReborn-1.12.2-12.4.11.jar                | 9f5e2a811a8332a842b34f6967b7db0ac4f24856 |
	| LCHIJA | thermaltinkering    | 1.0                      | ThermalTinkering-1.12.2-2.0.1.jar                 | None                                     |
	| LCHIJA | tinkersaether       | 1.3.0                    | tinkersaether-1.3.0.jar                           | None                                     |
	| LCHIJA | tcomplement         | 1.12.2-0.4.3             | TinkersComplement-1.12.2-0.4.3.jar                | None                                     |
	| LCHIJA | tinkersjei          | 1.2                      | tinkersjei-1.2.jar                                | None                                     |
	| LCHIJA | tinkertoolleveling  | 1.12.2-1.1.0.DEV.b23e769 | TinkerToolLeveling-1.12.2-1.1.0.jar               | None                                     |

	Loaded coremods (and transformers): 
Regeneration (Regeneration-1.12.2-3.0.2.jar)
  me.swirtzly.regeneration.asm.RegenClassTransformer
Inventory Tweaks Coremod (InventoryTweaks-1.63.jar)
  invtweaks.forge.asm.ContainerTransformer
MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.281.jar)
  micdoodle8.mods.miccore.MicdoodleTransformer
LoadingPlugin (ResourceLoader-MC1.12.1-1.5.3.jar)
  lumien.resourceloader.asm.ClassTransformer
	GL info: ' Vendor: 'ATI Technologies Inc.' Version: '4.6.14757 Compatibility Profile Context 20.12.1 27.20.14501.28009' Renderer: 'AMD Radeon R5 M435'
	Pulsar/tconstruct loaded Pulses: 
		- TinkerCommons (Enabled/Forced)
		- TinkerWorld (Enabled/Not Forced)
		- TinkerTools (Enabled/Not Forced)
		- TinkerHarvestTools (Enabled/Forced)
		- TinkerMeleeWeapons (Enabled/Forced)
		- TinkerRangedWeapons (Enabled/Forced)
		- TinkerModifiers (Enabled/Forced)
		- TinkerSmeltery (Enabled/Not Forced)
		- TinkerGadgets (Enabled/Not Forced)
		- TinkerOredict (Enabled/Forced)
		- TinkerIntegration (Enabled/Forced)
		- TinkerFluids (Enabled/Forced)
		- TinkerMaterials (Enabled/Forced)
		- TinkerModelRegister (Enabled/Forced)
		- wailaIntegration (Enabled/Not Forced)

	Pulsar/natura loaded Pulses: 
		- NaturaCommons (Enabled/Forced)
		- NaturaOverworld (Enabled/Not Forced)
		- NaturaNether (Enabled/Not Forced)
		- NaturaDecorative (Enabled/Not Forced)
		- NaturaTools (Enabled/Not Forced)
		- NaturaEntities (Enabled/Not Forced)
		- NaturaOredict (Enabled/Forced)
		- NaturaWorld (Enabled/Not Forced)

	Pulsar/tcomplement loaded Pulses: 
		- ModuleCommons (Enabled/Forced)
		- ModuleMelter (Enabled/Not Forced)
		- ModuleArmor (Enabled/Not Forced)
		- ModuleSteelworks (Enabled/Not Forced)
		- ToolLevelingPlugin (Enabled/Not Forced)
		- Oredict (Enabled/Forced)

	Launched Version: MultiMC5
	LWJGL: 2.9.4
	OpenGL: AMD Radeon R5 M435 GL version 4.6.14757 Compatibility Profile Context 20.12.1 27.20.14501.28009, ATI Technologies Inc.
	GL Caps: Using GL 1.3 multitexturing.
Using GL 1.3 texture combiners.
Using framebuffer objects because OpenGL 3.0 is supported and separate blending is supported.
Shaders are available because OpenGL 2.1 is supported.
VBOs are available because OpenGL 1.5 is supported.

	Using VBOs: Yes
	Is Modded: Definitely; Client brand changed to 'fml,forge'
	Type: Client (map_client.txt)
	Resource Packs: 
	Current Language: English (US)
	Profiler Position: N/A (disabled)
	CPU: 6x Intel(R) Core(TM) i5-8400 CPU @ 2.80GHz