---- Minecraft Crash Report ----
// There are four lights!

Time: 2021-03-27 13:44:29 CET
Description: Initializing game

org.lwjgl.LWJGLException: Pixel format not accelerated
    at org.lwjgl.opengl.WindowsPeerInfo.nChoosePixelFormat(Native Method)
    at org.lwjgl.opengl.WindowsPeerInfo.choosePixelFormat(WindowsPeerInfo.java:52)
    at org.lwjgl.opengl.WindowsDisplay.createWindow(WindowsDisplay.java:247)
    at org.lwjgl.opengl.Display.createWindow(Display.java:306)
    at org.lwjgl.opengl.Display.create(Display.java:848)
    at org.lwjgl.opengl.Display.create(Display.java:757)
    at org.lwjgl.opengl.Display.create(Display.java:739)
    at net.minecraft.client.Minecraft.createDisplay(Minecraft.java:645)
    at net.minecraft.client.Minecraft.init(Minecraft.java:458)
    at net.minecraft.client.Minecraft.run(Minecraft.java:3931)
    at net.minecraft.client.main.Main.main(SourceFile:123)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
    at java.lang.reflect.Method.invoke(Unknown Source)
    at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
    at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(Unknown Source)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(Unknown Source)
    at java.lang.reflect.Method.invoke(Unknown Source)
    at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:196)
    at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:231)
    at org.multimc.EntryPoint.listen(EntryPoint.java:143)
    at org.multimc.EntryPoint.main(EntryPoint.java:34)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
  Minecraft Version: 1.12.2
  Operating System: Windows 10 (amd64) version 10.0
  Java Version: 1.8.0_281, Oracle Corporation
  Java VM Version: Java HotSpot(TM) 64-Bit Server VM (mixed mode), Oracle Corporation
  Memory: 1548465880 bytes (1476 MB) / 1837629440 bytes (1752 MB) up to 6681526272 bytes (6372 MB)
  JVM Flags: 3 total; -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms520m -Xmx7168m
  IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
  FML: 
  Loaded coremods (and transformers): Regeneration (Regeneration-1.12.2-3.0.2.jar)
                                        me.swirtzly.regeneration.asm.RegenClassTransformer
                                      IELoadingPlugin (ImmersiveEngineering-core-0.12-98.jar)
                                        blusunrize.immersiveengineering.common.asm.IEClassTransformer
                                      RandomPatches (randompatches-1.12.2-1.22.1.10.jar)
                                        com.therandomlabs.randompatches.core.RPTransformer
                                      TransformerLoader (OpenComputers-MC1.12.2-1.7.5.192.jar)
                                        li.cil.oc.common.asm.ClassTransformer
                                      Do not report to Forge! (If you haven't disabled the FoamFix coremod, try disabling it in the config! Note that this bit of text will still appear.) (foamfix-0.10.11-1.12.2.jar)
                                        pl.asie.foamfix.coremod.FoamFixTransformer
                                      CTMCorePlugin (CTM-MC1.12.2-1.0.2.31.jar)
                                        team.chisel.ctm.client.asm.CTMTransformer
                                      SurgeLoadingPlugin (Surge-1.12.2-2.0.79.jar)
                                        
                                      Inventory Tweaks Coremod (InventoryTweaks-1.63.jar)
                                        invtweaks.forge.asm.ContainerTransformer
                                      MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.281.jar)
                                        micdoodle8.mods.miccore.MicdoodleTransformer
                                      VanillaFixLoadingPlugin (VanillaFix-1.0.10-150.jar)
                                        
                                      PhosphorFMLLoadingPlugin (phosphor-1.12.2-0.2.6+build50-universal.jar)
                                        
                                      JustEnoughIDs Extension Plugin (JustEnoughIDs-1.0.3-55.jar)
                                        org.dimdev.jeid.JEIDTransformer
                                      LoadingPlugin (ResourceLoader-MC1.12.1-1.5.3.jar)
                                        lumien.resourceloader.asm.ClassTransformer
                                      SteveKunGLibPlugin (SteveKunG's-Lib-1.12.2-1.1.10.jar)
                                        
                                      HCASM (HammerLib-1.12.2-2.0.6.26.jar)
                                        com.zeitheron.hammercore.asm.HammerCoreTransformer
  Suspected Mods: Unknown
  Launched Version: MultiMC5
  LWJGL: 2.9.4
  OpenGL: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
  GL Caps: 
  Using VBOs: Yes
  Is Modded: Definitely; Client brand changed to 'fml,forge'
  Type: Client (map_client.txt)
  Resource Packs: ctp_1.2_MCv3.zip, Aether b1.7.3 Textures
  Current Language: ~~ERROR~~ NullPointerException: null
  Profiler Position: N/A (disabled)
  CPU: <unknown>
