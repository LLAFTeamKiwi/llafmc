---- Minecraft Crash Report ----
// Who set us up the TNT?

Time: 2021-03-27 14:43:28 CET
Description: Initializing game

org.lwjgl.LWJGLException: Pixel format not accelerated
    at org.lwjgl.opengl.WindowsPeerInfo.nChoosePixelFormat(Native Method)
    at org.lwjgl.opengl.WindowsPeerInfo.choosePixelFormat(WindowsPeerInfo.java:52)
    at org.lwjgl.opengl.WindowsDisplay.createWindow(WindowsDisplay.java:247)
    at org.lwjgl.opengl.Display.createWindow(Display.java:306)
    at org.lwjgl.opengl.Display.create(Display.java:848)
    at org.lwjgl.opengl.Display.create(Display.java:757)
    at org.lwjgl.opengl.Display.create(Display.java:739)
    at net.minecraft.client.Minecraft.createDisplay(Minecraft.java:645)
    at net.minecraft.client.Minecraft.init(Minecraft.java:458)
    at net.minecraft.client.Minecraft.run(Minecraft.java:3931)
    at net.minecraft.client.main.Main.main(SourceFile:123)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:498)
    at net.minecraft.launchwrapper.Launch.launch(Launch.java:135)
    at net.minecraft.launchwrapper.Launch.main(Launch.java:28)
    at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
    at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
    at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
    at java.lang.reflect.Method.invoke(Method.java:498)
    at org.multimc.onesix.OneSixLauncher.launchWithMainClass(OneSixLauncher.java:196)
    at org.multimc.onesix.OneSixLauncher.launch(OneSixLauncher.java:231)
    at org.multimc.EntryPoint.listen(EntryPoint.java:143)
    at org.multimc.EntryPoint.main(EntryPoint.java:34)


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
  Minecraft Version: 1.12.2
  Operating System: Windows 8.1 (amd64) version 6.3
  Java Version: 1.8.0_282, AdoptOpenJDK
  Java VM Version: Eclipse OpenJ9 VM (JRE 1.8.0 Windows 8.1 amd64-64-Bit Compressed References 20210120_972 (JIT enabled, AOT enabled)
                   OpenJ9   - 345e1b09e
                   OMR      - 741e94ea8
                   JCL      - ab07c6a8fd based on jdk8u282-b08), Eclipse OpenJ9
  Memory: 307120912 bytes (292 MB) / 558104576 bytes (532 MB) up to 5234491392 bytes (4992 MB)
  JVM Flags: 6 total; -Xoptionsfile=C:\Program Files\AdoptOpenJDK\jdk-8.0.282.8-openj9\jre\bin\compressedrefs\options.default -Xlockword:mode=default,noLockword=java/lang/String,noLockword=java/util/MapEntry,noLockword=java/util/HashMap$Entry,noLockword=org/apache/harmony/luni/util/ModifiedMap$Entry,noLockword=java/util/Hashtable$Entry,noLockword=java/lang/invoke/MethodType,noLockword=java/lang/invoke/MethodHandle,noLockword=java/lang/invoke/CollectHandle,noLockword=java/lang/invoke/ConstructorHandle,noLockword=java/lang/invoke/ConvertHandle,noLockword=java/lang/invoke/ArgumentConversionHandle,noLockword=java/lang/invoke/AsTypeHandle,noLockword=java/lang/invoke/ExplicitCastHandle,noLockword=java/lang/invoke/FilterReturnHandle,noLockword=java/lang/invoke/DirectHandle,noLockword=java/lang/invoke/ReceiverBoundHandle,noLockword=java/lang/invoke/DynamicInvokerHandle,noLockword=java/lang/invoke/FieldHandle,noLockword=java/lang/invoke/FieldGetterHandle,noLockword=java/lang/invoke/FieldSetterHandle,noLockword=java/lang/invoke/StaticFieldGetterHandle,noLockword=java/lang/invoke/StaticFieldSetterHandle,noLockword=java/lang/invoke/IndirectHandle,noLockword=java/lang/invoke/InterfaceHandle,noLockword=java/lang/invoke/VirtualHandle,noLockword=java/lang/invoke/PrimitiveHandle,noLockword=java/lang/invoke/InvokeExactHandle,noLockword=java/lang/invoke/InvokeGenericHandle,noLockword=java/lang/invoke/VarargsCollectorHandle,noLockword=java/lang/invoke/ThunkTuple -Xjcl:jclse29 -XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -Xms520m -Xmx4992m
  IntCache: cache: 0, tcache: 0, allocated: 0, tallocated: 0
  FML: 
  Loaded coremods (and transformers): Regeneration (Regeneration-1.12.2-3.0.2.jar)
                                        me.swirtzly.regeneration.asm.RegenClassTransformer
                                      IELoadingPlugin (ImmersiveEngineering-core-0.12-98.jar)
                                        blusunrize.immersiveengineering.common.asm.IEClassTransformer
                                      RandomPatches (randompatches-1.12.2-1.22.1.10.jar)
                                        com.therandomlabs.randompatches.core.RPTransformer
                                      TransformerLoader (OpenComputers-MC1.12.2-1.7.5.192.jar)
                                        li.cil.oc.common.asm.ClassTransformer
                                      Do not report to Forge! (If you haven't disabled the FoamFix coremod, try disabling it in the config! Note that this bit of text will still appear.) (foamfix-0.10.11-1.12.2.jar)
                                        pl.asie.foamfix.coremod.FoamFixTransformer
                                      CTMCorePlugin (CTM-MC1.12.2-1.0.2.31.jar)
                                        team.chisel.ctm.client.asm.CTMTransformer
                                      SurgeLoadingPlugin (Surge-1.12.2-2.0.79.jar)
                                        
                                      Inventory Tweaks Coremod (InventoryTweaks-1.63.jar)
                                        invtweaks.forge.asm.ContainerTransformer
                                      MicdoodlePlugin (MicdoodleCore-1.12.2-4.0.2.281.jar)
                                        micdoodle8.mods.miccore.MicdoodleTransformer
                                      VanillaFixLoadingPlugin (VanillaFix-1.0.10-150.jar)
                                        
                                      PhosphorFMLLoadingPlugin (phosphor-1.12.2-0.2.6+build50-universal.jar)
                                        
                                      JustEnoughIDs Extension Plugin (JustEnoughIDs-1.0.3-55.jar)
                                        org.dimdev.jeid.JEIDTransformer
                                      LoadingPlugin (ResourceLoader-MC1.12.1-1.5.3.jar)
                                        lumien.resourceloader.asm.ClassTransformer
                                      SteveKunGLibPlugin (SteveKunG's-Lib-1.12.2-1.1.10.jar)
                                        
                                      HCASM (HammerLib-1.12.2-2.0.6.26.jar)
                                        com.zeitheron.hammercore.asm.HammerCoreTransformer
  Suspected Mods: Unknown
  Launched Version: MultiMC5
  LWJGL: 2.9.4
  OpenGL: ~~ERROR~~ RuntimeException: No OpenGL context found in the current thread.
  GL Caps: 
  Using VBOs: Yes
  Is Modded: Definitely; Client brand changed to 'fml,forge'
  Type: Client (map_client.txt)
  Resource Packs: ctp_1.2_MCv3.zip, Aether b1.7.3 Textures
  Current Language: ~~ERROR~~ NullPointerException: null
  Profiler Position: N/A (disabled)
  CPU: <unknown>
